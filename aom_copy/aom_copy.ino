/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

static inline void delayCycles(uint32_t) __attribute__((always_inline, unused));
static inline void delayCycles(uint32_t cycles)
{ // MIN return in 7 cycles NEAR 20 cycles it gives wait +/- 2 cycles - with sketch timing overhead
  uint32_t begin = ARM_DWT_CYCCNT-12; // Overhead Factor for execution
  while (ARM_DWT_CYCCNT - begin < cycles) ; // wait 
}

#define OUT_AOM 21
#define OUT_SHUTTER 9
#define IN_LIGHT A0
#define IN_HSYNC 23
#define IN_AOM A7
//#define IN_AOM 7

#define TEENY_LED 13

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(OUT_AOM, OUTPUT);
  pinMode(TEENY_LED, OUTPUT);
 // pinMode(IN_LIGHT, INPUT);
 // pinMode(IN_AOM, INPUT);
  
  // analogReference(EXTERNAL);  // set analog reference to internal ref
 // analogReadRes(8);          // Teensy 3.0: set ADC resolution to this many bits
  //analogReadAveraging(1);    // average this many readings

  //pinMode(LED_BUILTIN, OUTPUT);
  //digitalWrite(LED_BUILTIN, LOW);

  //Serial.begin(9600);
}

// https://forum.pjrc.com/threads/57683-Analog-read-on-Teensy-4-0-slower-(compared-to-Teensy-3-6)

void yield () {} //Get rid of the hidden function that checks for serial input and such.

FASTRUN void loop() 
// Use FASTRUN to force code to run in RAM.
{   
  int mode=0;
  int hstate=0;
  int value=0;
  
  noInterrupts();

  // First see what state Hsync is in and sync to it
  value=digitalReadFast(IN_HSYNC);
  while (value>0) {
      value=digitalReadFast(IN_HSYNC);
  }
  
  // Now we know HSYNC is low

  
 // int reading = analogRead(IN_LIGHT);
//  int aom_state = analogRead(IN_AOM);
//  
//  if (aom_state > 5) {
//    digitalWrite(OUT_AOM, 1);
//   // digitalWrite(LED_BUILTIN, LOW);
//  } else {
//    digitalWrite(OUT_AOM, 0);
//  //  digitalWrite(LED_BUILTIN, LOW);
//  }

 // Serial.println(aom_state);

 //   digitalWrite(OUT_AOM, HIGH);
//    digitalWrite(OUT_AOM, 1);   // turn the LED on (HIGH is the voltage level)
 //   digitalWrite(TEENY_LED, HIGH);   // turn the LED on (HIGH is the voltage level)
 //   delay(500);
 //   digitalWrite(OUT_AOM, 0);
 //   digitalWrite(TEENY_LED, LOW);   // turn the LED on (HIGH is the voltage level)
 //   digitalWrite(OUT_AOM, HIGH);   // turn the LED on (HIGH is the voltage level)
 //   delay(500);

  while (1) {
      digitalWriteFast(OUT_AOM,HIGH);
      delayCycles(10);
      digitalWriteFast(OUT_AOM,LOW);
      delayCycles(10);
  }
      
#ifdef MONITOR_MODE 
  // This works for stimulus computer in TSLO room (cheap monitor)
 // if (reading > 949)
 //   digitalWrite(OUT_SHUTTER, HIGH);   // turn the LED on (HIGH is the voltage level)
 // else
 //   digitalWrite(OUT_SHUTTER, LOW);    // turn the LED off by making the voltage LOW
//else

//    digitalWrite(OUT_SHUTTER, LOW);   // turn the LED on (HIGH is the voltage level)
//    delay(500);
//    digitalWrite(OUT_SHUTTER, HIGH);   // turn the LED on (HIGH is the voltage level)
 //   delay(500);
#endif

}
