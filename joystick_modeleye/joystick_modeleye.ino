#include <Servo.h>

const int servo1 = 9;       // first servo
const int servo2 = 10;       // second servo
const int joyH = 0;        // L/R Parallax Thumbstick
const int joyV = 1;        // U/D Parallax Thumbstick

const int laser_pin=2;
const int switch_pin=3;
const int switch2_pin=4;
const int switch3_pin=5;

#define CENTERX 140//96
#define CENTERY 117

float centerX=CENTERX; // approx ahead (opposite cables)
float centerY=CENTERY; // approx straight ahead

int servoVal;           // variable to read the value from the analog pin

Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo

#define MODE_INITIAL 0
#define MODE_CENTERED 1

int laser_mode;

int mode;

int center_ms;

void setup() {

  // Servo  
  myservo1.attach(servo1);  // attaches the servo
  myservo2.attach(servo2);  // attaches the servo

  // Inizialize Serial
  Serial.begin(9600);

  laser_mode=1;
  mode=MODE_CENTERED;
  
  pinMode( laser_pin, OUTPUT);
  digitalWrite(laser_pin, LOW);
  pinMode( switch_pin, INPUT);
  pinMode( switch2_pin, INPUT);
  pinMode( switch3_pin, INPUT);
   
  // Initial position::
  centerX=CENTERX  +  0;    // Enter X number here in place of "0", HIT CTRL-U
  centerY=CENTERY  + 0;    // Enter Y number here in place of "0"

  center_ms=map(CENTERX,0, 180, 544, 2400 );
  
  myservo2.write(centerX); 
  delay(10);  
  myservo1.write(centerY);  
  
  // Initial laser mode:
  if (laser_mode)
    digitalWrite(laser_pin, HIGH);
  else
    digitalWrite(laser_pin, LOW);  
}


void loop(){
    int sw;

    // TODO: Need to add debounce
    sw = digitalRead(switch_pin);
    if (sw == HIGH) {
      laser_mode=(laser_mode==0);
    }

    if (laser_mode)
      digitalWrite(laser_pin, HIGH);
    else
      digitalWrite(laser_pin, LOW);

    sw = digitalRead(switch2_pin);
    if (sw == HIGH) {
      mode=MODE_CENTERED;
    }
    sw = digitalRead(switch3_pin);
    if (sw == HIGH) {
        mode=MODE_INITIAL;
        myservo2.write(centerX);   
    }

    if (mode==MODE_CENTERED) {
        //myservo2.write(centerX+1);
        myservo2.writeMicroseconds(center_ms);   
        delay(2000);                            
        // Value in us: Servo class maps 0-180deg to 544-2400us, but
        // need to confirm empirically for each motor. For our decent Turnigy motor,
        // it seems close to desired 1 deg, or 10.37us/degree
        // Minimum value observable for Turnigy is around 3us (1/3 degree), although it's not pretty (symmetric)
        // See also VarSpeedServo library.
        myservo2.writeMicroseconds(center_ms+6); 
        //myservo2.write(centerX+2);   
        delay(2000);                                       
        goto last;
    }
        
    // Display Joystick values using the serial monitor
    // outputJoystick();

    // Read the horizontal joystick value  (value between 0 and 1023)
    servoVal = analogRead(joyH);          
    //servoVal = map(servoVal, 0, 1023, 0, 180);     // scale it to use it with the servo (result  between 0 and 180)
    
    if (servoVal>520)
      if (servoVal<2000)
        centerX += float(servoVal-520)/1000;

    if (servoVal<490)
      if (servoVal>-1)  //nop
        centerX += float(servoVal-490)/1000; // Will be negative
   
    // Motor does strange things when at large end of it range
    if (centerX>(85+CENTERX)) {
      centerX=(85+CENTERX); }
    if (centerX<0) {
      centerX=0;
      }
              
    myservo2.write(int(centerX));                         // sets the servo position according to the scaled value    

    // Read the horizontal joystick value  (value between 0 and 1023)
    servoVal = analogRead(joyV);

    if (servoVal>520)
      if (servoVal<2000) //nop
        centerY += float(servoVal-520)/1000;

    if (servoVal<490)
      if (servoVal>-1)
        centerY += float(servoVal-590)/1000;

    if (centerY<40)
      centerY=40;
    if (centerY>175)
      centerY=175;
      
    //servoVal = map(servoVal, 0, 1023, 0, 135);     // scale it to use it with the servo (result between 70 and 180)

    myservo1.write(int(centerY));                           // sets the servo position according to the scaled value

    output(2);
    delay(10);                                       // waits for the servo to get there

last:
    //delay(10);
    return;
}


/**
* Display joystick values
*
* which=0: nothing
* which=1: analog joystick values
* which=2: center (laser) positions
* which=3: both
*/

void output(int which){

  if ( (which%2) ==1) {
    Serial.print(analogRead(joyH));
    Serial.print ("--"); 
    Serial.print(analogRead(joyV));
    Serial.print ("-");
  };
  
  if (which>=2){
    Serial.print ("x=");
    Serial.print (int( centerX)-CENTERX);
    Serial.print (" y="); 
    Serial.print (int(centerY)-CENTERY);
    Serial.println (" * "); 
    //Serial.println ("----------------");
  };
}
