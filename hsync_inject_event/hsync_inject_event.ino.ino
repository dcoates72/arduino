/*
  hsync_inject: state machine for Teensy to control the AOM
  currently (2021-02-13) just set the AOM high during each line, unless we are in the middle of a detected event (beacon from stimulus computer)
*/

static inline void delayCycles(uint32_t) __attribute__((always_inline, unused));
static inline void delayCycles(uint32_t cycles)
{ // MIN return in 7 cycles NEAR 20 cycles it gives wait +/- 2 cycles - with sketch timing overhead
  uint32_t begin = ARM_DWT_CYCCNT-12; // Overhead Factor for execution
  while (ARM_DWT_CYCCNT - begin < cycles) ; // wait 
}

#define OUT_AOM 21
#define OUT_SHUTTER 9
#define IN_BEACON 22
#define IN_HSYNC 23
#define IN_AOM A7

#define IN_BUTTON 20
#define OUT_GREEN_STATUS 19

//#define IN_AOM 7

#define TEENY_LED 13

#define SHUTTER_LINES 10
#define LINES_BETWEEN_BEACONS 512*3

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(OUT_AOM, OUTPUT);
  pinMode(TEENY_LED, OUTPUT);
  pinMode(IN_HSYNC, INPUT);
  pinMode(IN_BEACON, INPUT);

  pinMode(IN_BUTTON, INPUT);
  pinMode(OUT_GREEN_STATUS, OUTPUT);
  
  //Serial.begin(9600);
}

#if 0
// https://forum.pjrc.com/threads/57683-Analog-read-on-Teensy-4-0-slower-(compared-to-Teensy-3-6)
// https://forum.pjrc.com/threads/57185-Teensy-4-0-Bitbang-FAST?p=212236&viewfull=1#post212236
// https://forum.pjrc.com/threads/54711-Teensy-4-0-First-Beta-Test?p=212280&viewfull=1#post212280
#endif //0

//void yield () {} //Get rid of the hidden function that checks for serial input and such.

FASTRUN void loop() 
// Use FASTRUN to force code to run in RAM.
{   
  int shutter_lines_left=0;
  int wait_lines_left=LINES_BETWEEN_BEACONS; // wait ~10 frames (330ms) before allowing another trigger
  int shuttered=0; // 0=ready to do one 1=clearing 16 lines, 2=done, don't clear anymore
  int value=0;
  int override_mode=0;
  unsigned int event_counter=0;

  override_mode=0;
  if (digitalReadFast(IN_BUTTON)==HIGH) {
    override_mode=1;
    digitalWriteFast(OUT_GREEN_STATUS,HIGH);
  }

  // Interrupts seem necessary for the elapsed micros routine
  //noInterrupts();

  // First see what state Hsync is in. Wait until it goes LOW
  value=digitalReadFast(IN_HSYNC);
  while (value==HIGH) { // (value>0) {
      value=digitalReadFast(IN_HSYNC);
  };
  // Now we know HSYNC is low

  // Here loops for each line (HSYNC)
  while (1) {
      // Wait until hsync goes hi:
      while (digitalReadFast(IN_HSYNC)==LOW) {
        // Make sure to look for BEACON while we are waiting for HSYNC
        if ((digitalReadFast(IN_BEACON)==LOW) && (shuttered==0) && (!override_mode) ) {
            shuttered=1;
            event_counter += 1;
            shutter_lines_left=SHUTTER_LINES;
        }
      }
      
      // Wait until hsync goes back LOW (full duration of blip)
      while (digitalReadFast(IN_HSYNC)==HIGH) {
        ; // hopefully don't need to worry about beacon here
      }

      // Wait ~
      delayCycles(980);

      // Here is the line start! Set AOM to high
      if (shuttered==1) {
          digitalWriteFast(OUT_AOM,LOW);
          shutter_lines_left-=1;
      } else {
        digitalWriteFast(OUT_AOM,HIGH);
      }

      elapsedMicros line_duration;
      while (line_duration<30) {
        if ((digitalReadFast(IN_BEACON)==LOW) && (shuttered==0) && (!override_mode)) {
            shuttered=1;
            event_counter += 1;
            shutter_lines_left=SHUTTER_LINES;
            digitalWriteFast(OUT_AOM,LOW); // beacon mid-line: turn off aom
        };

        // Put a "code" by turning on AOM at the beginning of each line 
        if ((shuttered==1) && (line_duration<17)) {
            // go one extra (until 17) to make sure it's turned off when get to 16, which is last
            int mode=LOW;
            // If we are between exactly "n" and "n-1" microseconds (of 30) on line,
            // turn the AOM for ~1us on to mark the event: will be 1-up code on raster
            if ( (line_duration>=(2*event_counter)) && (line_duration<(2*(event_counter+1))) ){
                mode=HIGH;
            };
            digitalWriteFast(OUT_AOM,mode); 
        };
      if (event_counter>8) {
        event_counter=0;
      };
     };
      //delayCycles(63); //not neeeded
    
      // That's the end of the line, now lower until next line...
      // Lower AOM
      digitalWriteFast(OUT_AOM,LOW);
          
      if ((shuttered==1) && (shutter_lines_left==0)) {
        shuttered=2; // this will be the last line off, so reset mode to do no more
        wait_lines_left=LINES_BETWEEN_BEACONS;
        digitalWriteFast(TEENY_LED,HIGH);
      }
    
      if (shuttered==2) {
         // Put a pause after we made the line to allow only one black strip every X often
         // (i.e., LINES_BETWEEN_BEACONS) lines
         wait_lines_left -=1;
         if (wait_lines_left==0) {
          // done waiting. can now safely re-arm for next trigger
          digitalWriteFast(TEENY_LED,LOW);
          shuttered=0;
          wait_lines_left=LINES_BETWEEN_BEACONS; // probably unnec. just in case.
        }
      }
  } // while (1)

}
