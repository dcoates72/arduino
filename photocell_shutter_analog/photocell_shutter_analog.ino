/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman  tty45      tty59      ttyS12     ttyS26     
tty19      tty32      tt

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

#define IN_PHOTOCELL A0
#define SHUTTER_OUTPUT 13

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(IN_PHOTOCELL, INPUT);
  pinMode(SHUTTER_OUTPUT, OUTPUT);

  Serial.begin(9600);
}

void loop() {
    int value;
    int threshold;
    value=analogRead(IN_PHOTOCELL);
    // Threshold would be different for each monitor
    // ASUS 240: 15
    // Predator (default): 2 (going from bloack to white patch) Also needs lights off otherwise gets false positives
    threshold=2;
//    Serial.println(value);
//    delay(1);
    if ( value<threshold) {
      digitalWrite(SHUTTER_OUTPUT, HIGH);   // turn the LED on (HIGH is the voltage level)
    } else {
      digitalWrite(SHUTTER_OUTPUT,LOW);
    }

}
