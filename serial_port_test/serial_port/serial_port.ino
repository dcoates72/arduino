#define TTL Serial1 // Serial1 or Serial3

/////////////////////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(9600L);
  Serial.println("ttl started");

  TTL.begin(115200L);
}

/////////////////////////////////////////////////////////////////////////////
void loop() {
  if (Serial.available()) {
    byte in = Serial.read();
    TTL.write(in);
  }

  if (TTL.available()) {
    byte in = TTL.read();
    Serial.write(in);
  }
}
