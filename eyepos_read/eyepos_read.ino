#define in_xpos A1
#define in_ypos A4
#define LED 13
//set of variable for the smoothing protocol
const int numReadings = 10;
int readings_x[numReadings];      // the readings from the analog input
int readings_y[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total_x = 0;                  // the running total
int total_y = 0;                  // the running total
int average_x = 0;                // the average
int average_y = 0;                // the average

// the setup function runs once when you press reset or power the board
void setup() {
  // put your setup code here, to run once:
  pinMode(in_xpos,INPUT);
  pinMode(in_ypos,INPUT);
  pinMode(LED,OUTPUT);
  // We set the analog reference voltage
  analogReference (INTERNAL);
  // initialize all the readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings_x[thisReading] = 0;
  }
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
    int value;
    int PrevXPos;
    int PrevYPos;
    int diff_x;
    int diff_y;
    int lower_thresh_x;
    int upper_thresh_x;
    unsigned long timeStamp;
    int radix = 10;
    char buffer[33];
    char *strptr;

    // We initial set the lower & upper threshold for microsaccade
    lower_thresh_x=85;
    upper_thresh_x=125;

    // subtract the last reading:
    total_x = total_x - readings_x[readIndex];
    total_y = total_y - readings_y[readIndex];
    // read from the sensor:
    readings_x[readIndex] = analogRead(in_xpos);
    readings_y[readIndex] = analogRead(in_ypos);
    // add the reading to the total:
    total_x = total_x + readings_x[readIndex];
    total_y = total_y + readings_y[readIndex];

    // advance to the next position in the array:
    readIndex = readIndex + 1;
  
    // if we're at the end of the array...
    if (readIndex >= numReadings) {
      // ...wrap around to the beginning:
      readIndex = 0;
    }

    // calculate the average:
    average_x = total_x / numReadings;
    average_y=total_y/numReadings;
    // Add a time stamp
    timeStamp=millis();
    // convert long to string
    strptr = ltoa(timeStamp,buffer,radix);
    
    // For debugging: Turn on LED when x reached certain value
//    int threshold_x_val=350; // would depend on reference frame
//    if ((average_x)>threshold_x_val) {
//      digitalWrite(LED,HIGH);
//    } else {
//      digitalWrite(LED,LOW);
//    }


    // send it to the computer as ASCII digits
    int numBytesLeft=Serial.availableForWrite();
    // would initially check if the buffer is full
    // and wait if it cant handle the subsequent message
    if (numBytesLeft>15){
      if ((timeStamp) % 2 ==0 ){ //Even number timestamp
        Serial.print(strptr);
        Serial.print("\t");
        
        Serial.print(int(average_x));
        Serial.print("\t");
    
        Serial.println(int(average_y));
        delayMicroseconds (500);
      }
    }else{
      delay(1);
    }

}
